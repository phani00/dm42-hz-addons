Default Apps
============

Use the "service_class" feature of Hubzilla to automatically activate System Apps (Features) and addons, set basic settings, and mark them as "pinned" or "featured" (i.e., starred).

As of version 1.1, the channel configuration for logged in local users is checked upon each page view to make sure that the 'default_addons' and the 'default_system_apps' are installed.  If they are not installed, the plugin installs them.  The plugin also checks to see that they are "featured" or "pinned" according to the settings in the "service_class" of the account.  Note, this is a changed in behavior from the previous version which required visiting /settings to have settings reset.

Basic feature/app settings can also be configured.  If settings for the System App or Addon already exist, they will not be overwritten.

The plugin uses the following top level settings in the service_class array:

default_addons, pinned_addons, featured_addons
default_system_apps, pinned_system_apps, featured_system_apps

"Featured" items appear in the hamburger menu.  Pinned items appear in the nav bar.

You can also set default settings (pconfig) if those settings do not exist already.  If the pconfig variables already exist in the database, they will not be overwritten by the 'default' settings.

EXAMPLE
=======

Add the CalDAV System App and set the "cal_first_day" feature option to true.  Mark the CalDAV app as both featured and pinned.:

```
App::$config['service_class']['my_service_class'] = Array(
'default_system_apps' => ['CalDAV' => [ 'feature' => [ 'cal_first_day' => 1 ] ] ],
'pinned_system_apps' => ['CalDAV'],
'featured_system_apps' => ['CalDAV']
);
```

EXAMPLE 2
=========
Add the Cart addon and 

```
App::$config['service_class']['my_service_class'] = Array(
'default_addons' => ['cart' => [ 'cart' => ['enable' => 1, 'enable_manual_payments' => 1, 'enable_test_catalog' => 1]]],
'pinned_addons' => ['cart'],
'featured_addons' => ['cart']
);
```


SETTING DEFAULT CHANNELS
=========================

To have channels automatically added for users in a service class, you use the 'default_connections' setting.

```
App::$config['service_class']['my_service_class'] = Array(
'default_connections' => ['newusers@myhub.com','usersupport@myhub.com']
);
```

NOTE: Channels will only be added ONCE.  If a user deletes a channel after it has been automatically added, it will not be re-added.

NOTICE ABOUT CLONED CHANNELS
============================
NOTE: Because this addon modifies channel settings without direct user intervention, use of this ADDON may result in seemingly unpredictable behavior with CLONED channels.  When an app is "installed" or a pconfig variable is changed on a local channel, it will also be updated on all the clones of that channel.  There are no known problems with this fact, but it is something to be aware of.
